import { useLayoutEffect } from "react";
import { useOAuth2Token } from "react-oauth2-hook";
import axios from "axios";
import { isSpotifyErrorResponse } from "../model/Search";

export function useAuth() {
  const [token, login, logout] = useOAuth2Token({
    authorizeUrl: "https://accounts.spotify.com/authorize",
    clientID: process.env.REACT_APP_SPOTIFY_CLIENT_ID!,
    redirectUri: window.location.origin + "/callback",
    scope: [],
  });

  useLayoutEffect(() => {
    if (!token) {
      return;
    }
    console.log("new token");

    const reqHandle = axios.interceptors.request.use((config) => {
      config.timeout = config.timeout ?? 1000;
      config.headers["Authorization"] = `Bearer ${token}`;
      return config;
    });

    const resHandle = axios.interceptors.response.use(
      (ok) => ok,
      (error) => {
        if (error instanceof axios.Cancel) {
          throw error;
        }

        if (!axios.isAxiosError(error)) {
          throw new Error("Unexpected error");
        }

        if (!error.response) {
          throw new Error("Check your internet connection!");
        }

        if (!isSpotifyErrorResponse(error.response.data)) {
          throw new Error("Unknown server response");
        }

        if (error.response.data.error.status === 401) {
          login();
          // return error.request
        }

        throw new Error(error.response.data.error.message);
      }
    );

    return () => {
      axios.interceptors.request.eject(reqHandle);
      axios.interceptors.response.eject(resHandle);
    };
  }, [token]);

  return [token, login, logout] as const;
}
