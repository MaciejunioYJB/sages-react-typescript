import { Album } from "./Search"

interface Entity {
    id: string;
    name: string;
}

export interface Playlist extends Entity {
    type: 'playlist'
    public: boolean;
    description: string;
    tracks?: Array<Track>
}

interface Track extends Entity {
    type: 'track';
    duration: number
}

type SearchResult = Track | Playlist // | Album


function parseResult2(result: SearchResult) {
    if ('duration' in result) {
        return `${result.name} - ${result.duration} ms`
    } else if ('description' in result && 'public' in result) {
        return `${result.name} - ${result.tracks?.length || 0} tracks`
    } else {
        exhaustivenessCheck(result)
    }

}

function parseResult(result: SearchResult): string {
    switch (result.type) {
        case 'playlist':
            return `${result.name} - ${result.tracks?.length || 0} tracks`
        case 'track':
            return `${result.name} - ${result.duration} ms`
        default:
            exhaustivenessCheck(result)
    }
}

parseResult({} as any).toLocaleLowerCase() // Object is possibly 'undefined'

function exhaustivenessCheck(_neverHappens: never): never {
    throw new Error('Unexpected value ' + _neverHappens)
}

// Type Guards

typeof {} == 'object';
[] instanceof Array;
// [] instanceof Playlist; // 'Playlist' only refers to a type, but is being used as a value here.
'duration' in { type: 'track', duration: 123 };
({} as Track | Playlist).type === 'track';
