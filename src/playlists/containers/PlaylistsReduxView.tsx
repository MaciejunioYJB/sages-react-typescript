import { useCallback, useEffect, useReducer } from "react";
import { playlistsMock } from "../../core/mocks/playlistsMock";
import { Dispatch } from "redux";
import {
  initialState,
  playlistsDelete,
  playlistSelectCurrent,
  playlistsFeature,
  playlistsLoad,
  playlistsLoadAsync,
  playlistsSelect,
  playlistsUpdate,
  reducer,
} from "../../core/reducers/Playlists";
import { PlaylistList } from "../components/PlaylistList";
import { useHistory, Route } from "react-router-dom";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { Playlist } from "../../core/model/Playlist";

import { useStore, useDispatch, useSelector } from "react-redux";
import { AppDispatch } from "../../core/store";

export const PlaylistsReduxView = () => {
  //   const [state, dispatch] = useReducer(reducer, initialState);

  const dispatch = useDispatch<AppDispatch>();
  //   const dispatch = useDispatch<any>();
  const { items: playlists, selectedId } = useSelector(playlistsFeature);
  const selectedPlaylist = useSelector(playlistSelectCurrent);

  const { push } = useHistory();

  useEffect(() => {
    (async () => {
      dispatch(playlistsLoadAsync);
      //   playlistsLoadAsync(dispatch);
      //   dispatch((dispatch)=>{
      //     dispatch({type:'PLAYLISTS_DELETE'})
      //   })
    })();
  }, []);

  const selectPlaylist = useCallback(
    (id: string, mode: "details" | "edit" = "details") => {
      dispatch(playlistsSelect(id));
      push(`/playlists/${id}/${mode}`);
    },
    []
  );

  const deletePlaylist = useCallback(
    (id: string) => dispatch(playlistsDelete(id)),
    []
  );

  const updatePlaylist = useCallback(
    (draft: Playlist) => dispatch(playlistsUpdate(draft)),
    []
  );

  return (
    <div>
      <div>
        <div className="row">
          <div className="col">
            <PlaylistList
              playlists={playlists}
              selected={selectedId}
              onSelect={selectPlaylist}
              onDelete={deletePlaylist}
            />
          </div>
          <div className="col">
            <Route
              path="/playlists/:playlist_id/details"
              render={({ match }) => {
                return (
                  selectedPlaylist && (
                    <PlaylistDetails
                      playlist={selectedPlaylist}
                      onEdit={() => selectPlaylist(selectedPlaylist.id, "edit")}
                    />
                  )
                );
              }}
            />
            <Route
              path="/playlists/:playlist_id/edit"
              render={({ match }) => {
                return (
                  selectedPlaylist && (
                    <PlaylistForm
                      playlist={selectedPlaylist}
                      onCancel={() =>
                        selectPlaylist(selectedPlaylist.id, "details")
                      }
                      onSave={updatePlaylist}
                    />
                  )
                );
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};
