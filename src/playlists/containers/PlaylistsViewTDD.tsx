import { useEffect, useState } from "react";
import { Playlist } from "../../core/model/Playlist";
import { fetchPlaylists } from "../../core/services/PlaylistsAPI";

interface Props {}

export const PlaylistsViewTDD = (props: Props) => {
  const [loading, setLoading] = useState(false);
  const [playlists, setPlaylists] = useState<Playlist[]>([]);

  useEffect(()=>{
    console.log('effect')
    loadPlaylists()
  },[])

  const loadPlaylists = async () => {
    setLoading(true);
    const results = await fetchPlaylists();
    setLoading(false);
    setPlaylists(results);
  };

  return (
    <div>
      {loading && (
        <p className="alert alert-info" role="alert" aria-label="Loading">
          Loading
        </p>
      )}
      {/* <button onClick={loadPlaylists}>Load Playlists</button> */}

      {playlists.map((p) => (
        <button role="tab" key={p.id}>
          {p.name}
        </button>
      ))}
    </div>
  );
};
