import {
  act,
  fireEvent,
  logRoles,
  render,
  screen,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import { PlaylistsViewTDD } from "./PlaylistsViewTDD";
import {
  fetchPlaylistById,
  fetchPlaylists,
} from "../../core/services/PlaylistsAPI";
import { Playlist } from "../../core/model/Playlist";
import { mocked } from "ts-jest/utils";

jest.mock("../../core/services/PlaylistsAPI");

// type Mocked<T extends (...args: any) => any> = jest.Mock<ReturnType< T>,Parameters<T>>

// function mocked<T extends (...args: any) => any>(fn:T):Mocked<T>{
//   return fn as unknown as Mocked<T>
// }

describe("PlaylistViewTDD", () => {
  const mockPlaylists: Playlist[] = [
    {
      id: "123",
      type: "playlist",
      name: "Playlist 123",
      public: true,
      description: "OPis...",
    },
    {
      id: "234",
      type: "playlist",
      name: "Playlist 234",
      public: false,
      description: "OPis.",
    },
    {
      id: "345",
      type: "playlist",
      name: "Playlist 345",
      public: true,
      description: "OPis...",
    },
  ];

  const setup = ({
    playlists = mockPlaylists,
    selected = playlists[0],
  }: {
    playlists?: Playlist[] | undefined;
    selected?: any;
  }) => {
    // (fetchPlaylistById as jest.Mock<Promise<Playlist | undefined>,[Playlist["id"]]>).mockResolvedValue(selected);
    // (fetchPlaylistById as jest.Mock<ReturnType<typeof fetchPlaylistById>,Parameters<typeof fetchPlaylistById>>).mockResolvedValue(selected);
    // (fetchPlaylistById as Mocked<typeof fetchPlaylistById>).mockResolvedValue(selected);

    mocked(fetchPlaylistById).mockResolvedValue(selected);

    (fetchPlaylists as jest.Mock).mockResolvedValue(playlists);

    render(<PlaylistsViewTDD />);
  };

  test("shows no playlists", async () => {
    setup({ playlists: [] });

    await waitForElementToBeRemoved(() =>
      screen.queryByRole("alert", { name: "Loading" })
    );

    const tab = screen.queryByRole("tab", {});
    expect(tab).not.toBeInTheDocument();
  });

  test("shows playlists from API", async () => {
    setup({});
    expect(fetchPlaylists).toHaveBeenCalled();

    await waitForElementToBeRemoved(() =>
      screen.queryByRole("alert", { name: "Loading" })
    );

    // logRoles(document.body);
    const tabs = await screen.findAllByRole("tab", {}, { timeout: 500 });
    expect(tabs).toHaveLength(3);
    expect(tabs[0]).toHaveTextContent(/Playlist 123/);
  });

  test("shows error loading playlists", () => {});

  test("shows selected playlist from API", () => {});
});
