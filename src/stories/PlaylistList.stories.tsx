import { ComponentStory, ComponentMeta } from "@storybook/react";

import { PlaylistList } from "../playlists/components/PlaylistList";

export default {
  title: "Playlists/List",
  component: PlaylistList,
  // argTypes: {
  //   backgroundColor: { control: "color" },
  // },
  decorators: [
    (Story) => (
      <div style={{ maxWidth: "200px", margin: "0 auto" }}>{Story()}</div>
    ),
  ],
} as ComponentMeta<typeof PlaylistList>;

const Template: ComponentStory<typeof PlaylistList> = (args) => {
  // const [selected, setSelected] = useState("234");
  // args = { ...args, onSelect: setSelected, selected };
  return <PlaylistList {...args} />;
};

export const EmptyList = Template.bind({});
EmptyList.args = {
  playlists: [],
};

export const SomePlaylists = Template.bind({});
SomePlaylists.args = {
  playlists: [
    { id: "123",type:'playlist', name: "Playlist 123", public: true, description: "OPis..." },
    { id: "234",type:'playlist', name: "Playlist 234", public: false, description: "OPis..." },
    { id: "345",type:'playlist', name: "Playlist 345", public: true, description: "OPis..." },
  ],
};
