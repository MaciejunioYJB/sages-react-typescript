import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";
import { MemoryRouter } from "react-router-dom";

test("renders learn react link", () => {
  render(
    /* <MockUserContextProvider> */
    <MemoryRouter>
      <App />
    </MemoryRouter>
    /* </MockUserContextProvider> */
  );
  const linkElement = screen.getByText(/Hello React/i);
  expect(linkElement).toBeInTheDocument();
});
