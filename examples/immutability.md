```ts

v1 = {
     tracks:[
         { id:1, author:{ name:'Author1' } },
         { id:2, author:{ name:'Author1' } },
    ]   
}
v2 = { 
    ...v1,
    tracks: v1.tracks.map(t => t.id === 1? {
        ...t, 
        author: { 
            ...t.author, 
            name:'Author2' 
        }
    } : t)
}

v1 == v2 
// false

v1.tracks == v2.tracks
// false

v1.tracks[0] == v2.tracks[0]
// false

v1.tracks[1] == v2.tracks[1]
// true

v1.tracks[0].author == v2.tracks[0].author
// false
```